package Exception;

public class WrongIDException extends Exception {
    public WrongIDException(Throwable cause) {
        super(cause);
    }
}
