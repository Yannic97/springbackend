package com.spring.spring.controller;

import com.spring.spring.model.Essen;
import com.spring.spring.service.EssenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/essen")
public class EssenController {
    @Autowired
    EssenService service;

    @GetMapping
    public List<Essen> getALlEssen() {
        return service.getAllEssen();
    }

    @GetMapping("/{id}")
    public Essen getEssen(@PathVariable int id) {
        return service.getEssen(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Essen insertEssen(@RequestBody Essen essen) {
        return service.insertEssen(essen);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteEssen(@PathVariable int id) {
        return service.deleteEssen(id);
    }

    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Essen updateEssen(@RequestBody Essen essen) {
        return service.updateEssen(essen);
    }
}
