package com.spring.spring.controller;

import com.spring.spring.model.EssenWithZutaten;
import com.spring.spring.service.EssenWithZutatenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/essenwithzutaten")
public class EssenWithZutatenController {
    @Autowired
    private EssenWithZutatenService service;
    @GetMapping("/{essenID}")
    public List<EssenWithZutaten>getEssenWithZutaten(@PathVariable int  essenID){
        return service.getEssenWithALlZutaten(essenID);
    }
}
