package com.spring.spring.controller;

import com.spring.spring.model.Essen_zutaten;
import com.spring.spring.service.Essen_zutatenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/essenzutaten")
public class Essen_zutatenController {
    @Autowired
    Essen_zutatenService service;

    @GetMapping
    public List<Essen_zutaten> getAllEssenZutaten(){
        return service.getAllEssenZutaten();
    }
    @GetMapping("/{id}")
    public Essen_zutaten getEssenZutatenByID(@PathVariable int id){
        return service.findEssenZutatenByID(id);
    }
    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Essen_zutaten updateEssenZutaten(@RequestBody Essen_zutaten essen_zutaten){
        return service.updateEssenZutaten(essen_zutaten);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Essen_zutaten insertEssenZutaten(@RequestBody Essen_zutaten essen_zutaten){
        return service.insertEssenZutaten(essen_zutaten);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deleteEssenZutaten(@PathVariable int id){
        return service.deleteEssen(id);
    }
}
