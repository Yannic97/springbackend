package com.spring.spring.controller;

import com.spring.spring.model.Zutaten;
import com.spring.spring.service.ZutatenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.annotation.WebServlet;
import java.util.List;

@RestController
@RequestMapping(value = "/zutaten")
public class ZutatenController {
    @Autowired
    ZutatenService service;

    @GetMapping
    public List<Zutaten> getAllZutaten() {
        return service.getAllZutaten();
    }

    @GetMapping(value = "/{id}")
    public Zutaten getZutateByID(@PathVariable int id) {
        return service.getZutatenByID(id);
    }

    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Zutaten updateZutaten(@RequestBody Zutaten zutaten) {
        return service.updateZutaten(zutaten);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Zutaten insertZutaten(@RequestBody Zutaten zutaten) {
        return service.insertZutat(zutaten);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteZutateByID(@PathVariable int id) {
        return service.deleteZutat(id);
    }

}
