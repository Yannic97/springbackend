package com.spring.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "essen")
public class Essen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer essenID;
    private String essennamen;
    private String beschreibung;
    private Double sterne;

    public Essen() {
    }


    public Essen(Integer essenID, String essenname, String beschreibung, Double sterne) {
        this.essenID = essenID;
        this.essennamen = essenname;
        this.beschreibung = beschreibung;
        this.sterne = sterne;
    }

    public Integer getEssenID() {
        return essenID;
    }

    public void setEssenID(Integer essenID) {
        this.essenID = essenID;
    }

    public String getEssenname() {
        return essennamen;
    }

    public void setEssenname(String essenname) {
        this.essennamen = essenname;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Double getSterne() {
        return sterne;
    }

    public void setSterne(Double sterne) {
        this.sterne = sterne;
    }
}
