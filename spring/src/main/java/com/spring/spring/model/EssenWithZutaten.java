package com.spring.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;
@Entity
public class EssenWithZutaten {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer essen_zutatenID;
    private String essennamen;
    private Integer essenID;
    private Integer zutatenID;
    private String zutatenname;
    private Double durchschnittspreis;

    public Integer getEssen_zutatenID() {
        return essen_zutatenID;
    }

    public void setEssen_zutatenID(Integer essen_zutatenID) {
        this.essen_zutatenID = essen_zutatenID;
    }

    public Integer getEssenID() {
        return essenID;
    }

    public void setEssenID(Integer essenID) {
        this.essenID = essenID;
    }

    public Integer getZutatenID() {
        return zutatenID;
    }

    public void setZutatenID(Integer zutatenID) {
        this.zutatenID = zutatenID;
    }

    public String getZutatenname() {
        return zutatenname;
    }

    public void setZutatenname(String zutatenname) {
        this.zutatenname = zutatenname;
    }

    public Double getDurchschnittspreis() {
        return durchschnittspreis;
    }

    public void setDurchschnittspreis(Double durchschnittspreis) {
        this.durchschnittspreis = durchschnittspreis;
    }

    public String getEssennamen() {
        return essennamen;
    }

    public void setEssennamen(String essennamen) {
        this.essennamen = essennamen;
    }

    public EssenWithZutaten(Integer essen_zutatenID, String essennamen, Integer essenID, Integer zutatenID, String zutatenname, Double durchschnittspreis) {
        this.essen_zutatenID = essen_zutatenID;
        this.essennamen = essennamen;
        this.essenID = essenID;
        this.zutatenID = zutatenID;
        this.zutatenname = zutatenname;
        this.durchschnittspreis = durchschnittspreis;
    }

    public EssenWithZutaten() {
    }
}
