package com.spring.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "essen_zutaten")
public class Essen_zutaten {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer essen_zutatenID;
    private Integer zutatenID;
    private Integer essenID;

    public Essen_zutaten(Integer essen_zutatenID, Integer zutatenID, Integer essenID) {
        this.essen_zutatenID = essen_zutatenID;
        this.zutatenID = zutatenID;
        this.essenID = essenID;
    }

    public Essen_zutaten() {
    }

    public Integer getEssen_zutatenID() {
        return essen_zutatenID;
    }

    public void setEssen_zutatenID(Integer essen_zutatenID) {
        this.essen_zutatenID = essen_zutatenID;
    }

    public Integer getZutatenID() {
        return zutatenID;
    }

    public void setZutatenID(Integer zutatenID) {
        this.zutatenID = zutatenID;
    }

    public Integer getEssenID() {
        return essenID;
    }

    public void setEssenID(Integer essenID) {
        this.essenID = essenID;
    }
}
