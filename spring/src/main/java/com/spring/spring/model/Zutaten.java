package com.spring.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "zutaten")
public class Zutaten {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer zutatenID;
    private String zutatenname;
    private Double durchschnittspreis;

    public Zutaten(Integer zutatenID, String zutatenname, Double durchschnittspreis) {
        this.zutatenID = zutatenID;
        this.zutatenname = zutatenname;
        this.durchschnittspreis = durchschnittspreis;
    }

    public Zutaten() {
    }

    public Integer getZutatenID() {
        return zutatenID;
    }

    public void setZutatenID(Integer zutatenID) {
        this.zutatenID = zutatenID;
    }

    public String getZutatenname() {
        return zutatenname;
    }

    public void setZutatenname(String zutatenname) {
        this.zutatenname = zutatenname;
    }

    public Double getDurchschnittspreis() {
        return durchschnittspreis;
    }

    public void setDurchschnittspreis(Double durchschnittspreis) {
        this.durchschnittspreis = durchschnittspreis;
    }
}
