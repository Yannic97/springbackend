package com.spring.spring.repository;

import com.spring.spring.model.Essen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EssenRepository extends JpaRepository<Essen, Integer> {
}
