package com.spring.spring.repository;

import com.spring.spring.model.EssenWithZutaten;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EssenWithZutatenRepository extends JpaRepository<EssenWithZutaten, Integer> {
    @Query(value = "SELECT essen_zutaten.essen_zutatenID,essen.essennamen, essen_zutaten.essenID,essen_zutaten.zutatenID, zutaten.zutatenname,zutaten.durchschnittspreis FROM essen_zutaten\n" +
            "INNER JOIN essen ON essen_zutaten.essenID=essen.essenID \n" +
            "INNER JOIN  zutaten ON essen_zutaten.zutatenID=zutaten.zutatenID\n" +
            "WHERE essen_zutaten.essenID= :id", nativeQuery = true)
    public List<EssenWithZutaten> getEssenWithZutatenALlInfoByID(int id);
}
