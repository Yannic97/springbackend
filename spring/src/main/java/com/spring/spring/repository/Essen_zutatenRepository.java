package com.spring.spring.repository;

import com.spring.spring.model.Essen_zutaten;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Essen_zutatenRepository extends JpaRepository<Essen_zutaten,Integer> {
}
