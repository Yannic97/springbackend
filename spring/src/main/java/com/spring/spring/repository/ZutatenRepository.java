package com.spring.spring.repository;

import com.spring.spring.model.Zutaten;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ZutatenRepository extends JpaRepository<Zutaten,Integer> {
}
