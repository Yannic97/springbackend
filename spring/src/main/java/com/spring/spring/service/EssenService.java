package com.spring.spring.service;

import com.spring.spring.model.Essen;
import com.spring.spring.repository.EssenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EssenService {
    @Autowired
    EssenRepository repository;

    public List<Essen> getAllEssen() {
        return repository.findAll();
    }

    public Essen getEssen(int id) {
        return repository.findById(id).get();
    }

    public Essen insertEssen(Essen essen) {
        essen = repository.save(essen);
        repository.flush();
        return essen;
    }

    public ResponseEntity deleteEssen(int id) {
        try {
            if (getEssen(id) == null)
                throw new IllegalArgumentException();
            repository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

    }

    public Essen updateEssen(Essen updateValue) {
        Essen essen = getEssen(updateValue.getEssenID());
        if (updateValue.getEssenname() != null) {
            essen.setEssenname(updateValue.getEssenname());
        }
        if (updateValue.getBeschreibung() != null) {
            essen.setBeschreibung(updateValue.getBeschreibung());
        }
        if (updateValue.getSterne() != null) {
            essen.setSterne(updateValue.getSterne());
        }
        repository.save(essen);
        return essen;
    }

}
