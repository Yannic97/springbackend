package com.spring.spring.service;

import com.spring.spring.model.EssenWithZutaten;
import com.spring.spring.repository.EssenWithZutatenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EssenWithZutatenService {
    @Autowired
    private EssenWithZutatenRepository repository;

    public List<EssenWithZutaten> getEssenWithALlZutaten(int id){
        return repository.getEssenWithZutatenALlInfoByID(id);
    }

}
