package com.spring.spring.service;

import com.spring.spring.model.Essen_zutaten;
import com.spring.spring.repository.Essen_zutatenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Essen_zutatenService {
    @Autowired
    private Essen_zutatenRepository repository;

    public List<Essen_zutaten>getAllEssenZutaten(){
        return repository.findAll();
    }
    public Essen_zutaten findEssenZutatenByID(int id){
        return repository.findById(id).get();
    }
    public Essen_zutaten insertEssenZutaten(Essen_zutaten essen_zutaten){
        essen_zutaten=repository.save(essen_zutaten);
        repository.flush();
        return essen_zutaten;
    }
    public Essen_zutaten updateEssenZutaten(Essen_zutaten updateValue){
        Essen_zutaten essen_zutatenDB=findEssenZutatenByID(updateValue.getEssen_zutatenID());
        if (updateValue.getEssenID()!=null){
            essen_zutatenDB.setEssenID(updateValue.getEssenID());
        }
        if (updateValue.getZutatenID()!=null){
            essen_zutatenDB.setZutatenID(updateValue.getZutatenID());
        }
        repository.save(essen_zutatenDB);
        return essen_zutatenDB;
    }
    public ResponseEntity deleteEssen(int id) {
        try {
            if (findEssenZutatenByID(id) == null)
                throw new IllegalArgumentException();
            repository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

    }
}
