package com.spring.spring.service;

import com.spring.spring.model.Zutaten;
import com.spring.spring.repository.ZutatenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZutatenService {
    @Autowired
    ZutatenRepository repository;

    public List<Zutaten> getAllZutaten() {
        return repository.findAll();
    }

    public Zutaten getZutatenByID(int id) {
        return repository.findById(id).get();
    }

    public Zutaten insertZutat(Zutaten insertValue) {
        insertValue = repository.save(insertValue);
        repository.flush();
        return insertValue;
    }

    public ResponseEntity deleteZutat(int id) {
        try {
            if (getZutatenByID(id) == null)
                throw new IllegalArgumentException();
            repository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    public Zutaten updateZutaten(Zutaten updateValue) {
        Zutaten zutatenDB = getZutatenByID(updateValue.getZutatenID());
        if (updateValue.getZutatenname() != null) {
            zutatenDB.setZutatenname(updateValue.getZutatenname());
        }
        if (updateValue.getDurchschnittspreis() != null) {
            zutatenDB.setDurchschnittspreis(updateValue.getDurchschnittspreis());
        }
        repository.save(zutatenDB);
        return zutatenDB;
    }
}
